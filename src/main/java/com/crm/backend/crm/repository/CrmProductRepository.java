package com.crm.backend.crm.repository;


import com.crm.backend.crm.model.CrmProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CrmProductRepository extends JpaRepository<CrmProduct, Long> {

}
