package com.crm.backend.crm.repository;


import com.crm.backend.crm.model.CrmNote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CrmNoteRepository extends JpaRepository<CrmNote, Long>{

    Optional<CrmNote> findCrmNoteByDescription(String description);
    Optional<CrmNote> findCrmNoteByNoteId(Long noteId);
    Page<CrmNote> findAllBy (Pageable pageable);
}
