package com.crm.backend.crm.repository;

import com.crm.backend.crm.model.CrmStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrmStatusRepository extends JpaRepository<CrmStatus, Long> {
}
