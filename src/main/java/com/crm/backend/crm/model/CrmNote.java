package com.crm.backend.crm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmNote {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long noteId;

    private String description;

    @OneToOne
    private CrmContactType crmContactType;

    public CrmNote(String description) {
        this.description = description;
    }
}
