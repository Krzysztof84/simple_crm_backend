package com.crm.backend.crm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CrmReminder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long reminderId;

    private LocalDate contactDate;
    private String reminderNote;
    private boolean isChecked = false;

    public CrmReminder(LocalDate contactDate, String reminderNote) {
        this.contactDate = contactDate;
        this.reminderNote = reminderNote;
    }

    public CrmReminder(LocalDate contactDate, String reminderNote, boolean isChecked) {
        this.contactDate = contactDate;
        this.reminderNote = reminderNote;
        this.isChecked = isChecked;
    }
}
