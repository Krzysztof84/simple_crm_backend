package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.exceptions.UserDoNotExistException;
import com.crm.backend.crm.exceptions.UserEmailAlreadyExistsException;
import com.crm.backend.crm.exceptions.UserLoginAlreadyExistsException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.dto.LoginDto;
import com.crm.backend.crm.model.dto.PageResponse;
import com.crm.backend.crm.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class AppUserService implements IAppUserService {

    private AppUserRepository appUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private static final int DEFAULT_PAGE_SIZE = 10;


    @Autowired
    public AppUserService(AppUserRepository appUserRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.appUserRepository = appUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void register(AppUser appUser) throws RegistrationException {
        appUser.setEmail(appUser.getEmail().toLowerCase());
        appUser.setLogin(appUser.getLogin().toLowerCase());


        Optional<AppUser> emailUser = appUserRepository.findByEmail(appUser.getEmail());
        if (emailUser.isPresent()) {
            throw new UserEmailAlreadyExistsException();
        }
        Optional<AppUser> loginUser = appUserRepository.findByLogin(appUser.getLogin());
        if (loginUser.isPresent()) {
            throw new UserLoginAlreadyExistsException();
        }

        appUser.setPassword(bCryptPasswordEncoder.encode(appUser.getPassword()));
        appUserRepository.save(appUser);
    }

    public PageResponse<AppUser> getAllUsers() {
        return getUsers(0);
    }

    public PageResponse<AppUser> getUsers(int page) {
        Page<AppUser> users = appUserRepository.findAllBy(PageRequest.of(page, DEFAULT_PAGE_SIZE));

        return new PageResponse<>(users);
    }

    public List<CrmClient> getAllClientsOfUser(Long userID) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(userID);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();
            return new ArrayList<>(new HashSet<>(appUser.getCrmClientList()));
        }
        return new ArrayList<>();
    }

    @Override
    public Optional<AppUser> getUserById(Long userId) {
        return appUserRepository.findById(userId);

    }

    @Override
    public void deleteUser(AppUser appUser) {
        appUserRepository.delete(appUser);
    }

    @Override
    public Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) throws UserDoNotExistException {
        Optional<AppUser> foundUser = appUserRepository.findByLogin(
                dto.getUsername());

        if (!foundUser.isPresent()) {
            throw new UserDoNotExistException();
        }else {

            AppUser user = foundUser.get(); // wydobywam uzytkownika
            // sprawdzam (nizej) czy hasło zgadza się z tym z bazy danych
            if (!bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword())) {
                //jesli się nie zgadza hasło to exception
                throw new UserDoNotExistException();
                //jeśli się zgadza to pomijam i zakonczy metode
            }

        }

        return foundUser;
    }


//     TODO: PAGE RESPONSE
//    public PageResponse<CrmClient> getAllClientsPage(int page, Long userID){
//        Page<CrmClient> clients = appUserRepository.findAllBy(PageRequest.of(page,DEFAULT_PAGE_SIZE));
//        return new PageResponse<>(clients);
//    }
//
}
