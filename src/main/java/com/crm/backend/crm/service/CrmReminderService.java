package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.CaseAlreadyExistsException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.exceptions.ReminderAlreadyExistsException;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.model.CrmReminder;
import com.crm.backend.crm.repository.CrmReminderRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CrmReminderService implements ICrmReminderService {

    private CrmReminderRepository crmReminderRepository;

    public CrmReminderService(CrmReminderRepository crmReminderRepository) {
        this.crmReminderRepository = crmReminderRepository;
    }

    @Override
    public void addReminder(CrmReminder crmReminder) throws RegistrationException {
        crmReminder.setReminderNote(crmReminder.getReminderNote().toLowerCase());

        Optional<CrmReminder> crmReminderOptional = crmReminderRepository.findCrmReminderByReminderNote(crmReminder.getReminderNote());
        if (crmReminderOptional.isPresent()) {
        }
        crmReminderRepository.save(crmReminder);

    }

    @Override
    public Optional<CrmReminder> createReminder(CrmReminder crmReminder) throws ReminderAlreadyExistsException {
        crmReminder.setReminderNote(crmReminder.getReminderNote());
        crmReminder.setContactDate(crmReminder.getContactDate());
        Optional<CrmReminder> crmReminderOptional = crmReminderRepository.findCrmReminderByReminderNote(crmReminder.getReminderNote());

        if (crmReminderOptional.isPresent()) {
            throw new ReminderAlreadyExistsException();
        }
        crmReminder = crmReminderRepository.save(crmReminder);
        return Optional.of(crmReminder);

    }

    @Override
    public void deleteReminder(CrmReminder crmReminder) {
        crmReminderRepository.delete(crmReminder);
    }
}
