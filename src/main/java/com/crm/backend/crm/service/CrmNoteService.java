package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.NoteAlreadyExistException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmNote;
import com.crm.backend.crm.repository.CrmNoteRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CrmNoteService implements ICrmNoteService {



    private CrmNoteRepository crmNoteRepository;

    public CrmNoteService(CrmNoteRepository crmNoteRepository) {this.crmNoteRepository = crmNoteRepository;}

    @Override
    public void addNote(CrmNote crmNote) throws RegistrationException{
        crmNote.setDescription(crmNote.getDescription().toLowerCase());

        Optional<CrmNote> crmNoteOptional = crmNoteRepository.findCrmNoteByDescription(crmNote.getDescription());
        if(crmNoteOptional.isPresent()){
        }
        crmNoteRepository.save(crmNote);

    }

    @Override
    public Optional<CrmNote> createNote(CrmNote crmNote) throws NoteAlreadyExistException {
        crmNote.setDescription(crmNote.getDescription());

        Optional<CrmNote> crmNoteOptional = crmNoteRepository.findCrmNoteByDescription(crmNote.getDescription());

        if (crmNoteOptional.isPresent()) {
            throw new NoteAlreadyExistException();

        }
        crmNote = crmNoteRepository.save(crmNote);
        return Optional.of(crmNote);
    }

    @Override
    public void deleteNote(CrmNote crmNote) {
        crmNoteRepository.delete(crmNote);
    }


}