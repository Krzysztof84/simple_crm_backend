package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.exceptions.ReminderAlreadyExistsException;
import com.crm.backend.crm.model.CrmReminder;

import java.util.Optional;

public interface ICrmReminderService {


    void addReminder(CrmReminder crmReminder) throws RegistrationException;

    Optional<CrmReminder> createReminder(CrmReminder crmReminder) throws ReminderAlreadyExistsException;

    void deleteReminder(CrmReminder crmReminder);
}
