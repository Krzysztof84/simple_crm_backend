package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.CaseAlreadyExistsException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmCase;

import java.util.List;
import java.util.Optional;

public interface ICrmCaseService {
    void addCase(CrmCase crmCase) throws RegistrationException;

    Optional<CrmCase> createCase(CrmCase crmCase)  throws RegistrationException;


    void deleteCase(CrmCase crmCase);
}
