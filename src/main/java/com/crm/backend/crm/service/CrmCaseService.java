package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.CaseAlreadyExistsException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.repository.CrmCaseRepository;
import com.crm.backend.crm.repository.CrmClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CrmCaseService implements ICrmCaseService{

    @Autowired
    private CrmCaseRepository crmCaseRepository;
    @Autowired
    private CrmClientRepository crmClientRepository;

    @Autowired
    public CrmCaseService(CrmCaseRepository crmCaseRepository) {
        this.crmCaseRepository = crmCaseRepository;
    }

    @Override
    public void addCase(CrmCase crmCase) throws RegistrationException {
        crmCase.setCaseDescription(crmCase.getCaseDescription());

        Optional<CrmCase> crmCaseOptional = crmCaseRepository.findCrmCaseByCaseDescription(crmCase.getCaseDescription());
        if (crmCaseOptional.isPresent()){
            throw new CaseAlreadyExistsException();
        }
        crmCaseRepository.save(crmCase);
    }


    @Override
    public Optional<CrmCase> createCase(CrmCase crmCase) throws RegistrationException {
        crmCase.setCaseDescription(crmCase.getCaseDescription());

        Optional<CrmCase> crmCaseOptional = crmCaseRepository.findCrmCaseByCaseDescription(crmCase.getCaseDescription());

        if (crmCaseOptional.isPresent()){
            throw new CaseAlreadyExistsException();
        }
        crmCase = crmCaseRepository.save(crmCase);
        return Optional.of(crmCase);
    }

    @Override
    public void deleteCase(CrmCase crmCase) {
        crmCaseRepository.delete(crmCase);
    }

}
