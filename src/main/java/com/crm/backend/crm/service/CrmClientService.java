package com.crm.backend.crm.service;

import com.crm.backend.crm.exceptions.ClientNameAlreadyExistsException;
import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.CrmReminder;
import com.crm.backend.crm.repository.CrmClientRepository;
import com.crm.backend.crm.repository.CrmReminderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CrmClientService implements ICrmClientService {

    private CrmClientRepository crmClientRepository;
    private AppUserService appUserService;

    @Autowired
    private CrmReminderRepository crmReminderRepository;

    public CrmClientService(CrmClientRepository crmClientRepository) {
        this.crmClientRepository = crmClientRepository;
    }


    @Override
    public Optional<CrmClient> register(CrmClient crmClient) throws RegistrationException {
        crmClient.setClientName(crmClient.getClientName().toLowerCase());

        Optional<CrmClient> crmClientOptional = crmClientRepository.findByClientName(crmClient.getClientName());
        if (crmClientOptional.isPresent()) {
            throw new ClientNameAlreadyExistsException();
        }
        crmClient = crmClientRepository.save(crmClient);
        return Optional.of(crmClient);
    }


    @Override
    public void deleteClient(CrmClient crmClient) {
        crmClientRepository.delete(crmClient);
    }

    @Override
    public CrmClient getCasesOfClient(Long clientId) {
        return crmClientRepository.findByClientId(clientId).get();
    }



}
