package com.crm.backend.crm.controller;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmCase;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.CrmStatus;
import com.crm.backend.crm.model.dto.Response;
import com.crm.backend.crm.model.dto.ResponseFactory;
import com.crm.backend.crm.repository.CrmCaseRepository;
import com.crm.backend.crm.repository.CrmClientRepository;
import com.crm.backend.crm.service.CrmCaseService;
import com.crm.backend.crm.service.ICrmCaseService;
import com.crm.backend.crm.service.ICrmClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/case/")
public class CrmCaseController {
    @Autowired
    private ICrmCaseService crmCaseService;
    @Autowired
    private CrmCaseRepository crmCaseRepository;
    @Autowired
    private CrmClientRepository crmClientRepository;
    @Autowired
    public CrmCaseController(ICrmCaseService crmCaseService) {
        this.crmCaseService = crmCaseService;
    }

//    @RequestMapping(path = "/addcase/", method = RequestMethod.POST)
//    public ResponseEntity<Response> addCase(@RequestBody CrmCase crmCase) {
//        try {
//            crmCaseService.addCase(crmCase);
//
//        } catch (RegistrationException e) {
//            return ResponseFactory.badRequest();
//        } return ResponseFactory.created();
//    }

    @RequestMapping (path = "/addcase", method = RequestMethod.GET)
    public ResponseEntity addCase(@RequestParam("clientid") Long clientId,
                                  @RequestParam("caseid") Long caseId){

        Optional<CrmClient> crmClientOptional = crmClientRepository.findById(clientId);
        if (crmClientOptional.isPresent()){
            CrmClient crmClient = crmClientOptional.get();

            Optional<CrmCase> crmCaseOptional = crmCaseRepository.findById(caseId);
            if (crmCaseOptional.isPresent()){
                CrmCase crmCase = crmCaseOptional.get();

                crmClient.getCrmCaseList().add(crmCase);
                crmClientRepository.save(crmClient);

                return ResponseFactory.created();
            }
        }
        return ResponseFactory.badRequest();
    }


    @RequestMapping(path = "/createcase", method = RequestMethod.GET)
    public ResponseEntity createCase(@RequestParam("casedescription") String caseDescription
                                   ) throws RegistrationException {
        Optional<CrmCase> addingCase = crmCaseService.createCase(new CrmCase(caseDescription));
        if(addingCase.isPresent()) {
            return ResponseFactory.ok(addingCase.get().getCaseId() + "");
        }else{
            return ResponseFactory.badRequest();
        }
    }

    @RequestMapping(path = "/deletecase", method = RequestMethod.DELETE)
    public void deleteCase(@RequestParam("caseid") Long caseid){

        Optional<CrmCase> caseOptional = crmCaseRepository.findById(caseid);
        if (caseOptional.isPresent()){
            CrmCase crmCase = caseOptional.get();
            crmCaseService.deleteCase(crmCase);
        }
    }


}
