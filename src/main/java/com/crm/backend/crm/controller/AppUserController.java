package com.crm.backend.crm.controller;

import com.crm.backend.crm.exceptions.RegistrationException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.CrmClient;
import com.crm.backend.crm.model.dto.PageResponse;
import com.crm.backend.crm.model.dto.Response;
import com.crm.backend.crm.model.dto.ResponseFactory;
import com.crm.backend.crm.repository.AppUserRepository;
import com.crm.backend.crm.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/user/")
public class AppUserController {
    @Autowired
    private AppUserService appUserService;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    public AppUserController(AppUserService appUserService){
        this.appUserService = appUserService;
    }


    @RequestMapping(path =  "/register", method = RequestMethod.POST)
    public ResponseEntity<Response> register(@RequestBody AppUser appUser){
        try {
            appUserService.register(appUser);
        } catch(RegistrationException e){
            return ResponseFactory.badRequest();
        }
        return ResponseFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity<Response> list(){
        PageResponse<AppUser> list = appUserService.getAllUsers();
        return ResponseFactory.result(list);

    }

    @RequestMapping(path = "/clientlist", method = RequestMethod.GET)
    public ResponseEntity<List<CrmClient>> clientList (@RequestParam ("userid") Long userId){
        List<CrmClient> clientList = appUserService.getAllClientsOfUser(userId);
        return ResponseFactory.result(clientList);
    }

    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
    public void deleteAppUser(@RequestParam ("userid") Long userId){
        Optional<AppUser> appUserOptional = appUserRepository.findById(userId);
        if (appUserOptional.isPresent()) {
            AppUser appUser = appUserOptional.get();
            appUserService.deleteUser(appUser);
        }
    }



}
