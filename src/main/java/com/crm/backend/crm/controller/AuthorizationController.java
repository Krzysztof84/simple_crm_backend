package com.crm.backend.crm.controller;

import com.crm.backend.crm.exceptions.UserDoNotExistException;
import com.crm.backend.crm.model.AppUser;
import com.crm.backend.crm.model.Role;
import com.crm.backend.crm.model.dto.AuthenticationDto;
import com.crm.backend.crm.model.dto.LoginDto;
import com.crm.backend.crm.model.dto.ResponseFactory;
import com.crm.backend.crm.service.IAppUserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.crm.backend.crm.configuration.JWTFilter.AUTHORITIES_KEY;
import static com.crm.backend.crm.configuration.JWTFilter.SECRET;

@RestController
@CrossOrigin
public class AuthorizationController {

    @Autowired
    private IAppUserService appUserService;

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<AuthenticationDto> authenticate(@RequestBody LoginDto dto) {
        try {
            Optional<AppUser> appUserOptional = appUserService.getUserWithLoginAndPassword(dto);
            AppUser user = appUserOptional.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            //We will sign our JWT with our ApiKey secret
            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setIssuedAt(new Date())
                    .claim(AUTHORITIES_KEY, translateRoles(user.getRoleSet())) // todo: do zmiany na getRoles?
                    .signWith(signatureAlgorithm, signingKey)
                    .compact();
            return ResponseFactory.result(new AuthenticationDto(token, user));
        } catch (UserDoNotExistException e) {
            e.printStackTrace();
        }
        return ResponseFactory.badRequest();
    }
    private Set<String> translateRoles(Set<Role> roles) {
        return roles.stream().map(role -> role.getName()).collect(Collectors.toSet());
    }
}
