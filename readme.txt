Simple CRM Program - backend Repo (working with frontend : https://bitbucket.org/mkeslinke/simple_crm_frontend/src/Marcin/)

 Project uses Spring framework, relational databases, MVCS pattern and AngularJS on frontend side.

 Feauters:
 User - add/delete
 Clients - add/delete/edit | add/delete : reminders, cases, notes

 Final project for Java course at SDA in Gdańsk, created by Marcin Keslinke&Krzysztof Styn